<?php

require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Name : $sheep->name <br>"; // "shaun"
echo "Legs count : $sheep->legs <br>"; // 2
echo "Blood type cold : $sheep->cold_blooded <br><br>";// false

$kodok = new Frog("buduk");
echo "Name : $kodok->name <br>";
echo "Legs count : $kodok->legs <br>";
echo "Blood type cold : $kodok->cold_blooded <br>";
$kodok->jump();
echo "<br><br>";

$sungokong = new ape("Kera Sakti");
echo "Name : $sungokong->name<br>";
echo "Legs count : $sungokong->legs<br>";
echo "Blood type cold : $sungokong->cold_blooded<br>";
$sungokong->yell();

?>